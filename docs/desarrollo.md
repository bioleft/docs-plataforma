La plataforma web es un desarrollo hecho con:

- Un backend desarrollado usando Fastapi. Los modelos, las rutas y la lógica están dentro de /backend.
- Un frontend desarrollado usando Svelte Kit. El código está en /frontend

Los usuarios en los que se basan los casos son *todos* en general, pero particularmente el sistema está diseñado para agricultores (familiares, urbanos y todos los que existan), mejoradores y productores.

## Configuración de entorno de desarrollo

Requerimientos:

- Postgresql
- Python
- Poetry
- Nodejs
- Yarn
- nvm (opcional)

**Backend:**

Primero, es necesario establecer la configuración de ambiente en ``` .env ```:

```
POSTGRES_DB="bioleft"
POSTGRES_NAME="bioleft"
POSTGRES_USER="bioleft"
POSTGRES_PASSWORD="bioleft"
POSTGRES_HOST="127.0.0.1"
POSTGRES_PORT="5432"
SERVER="localhost"
STATIC_FOLDER='localhost/static'
API_SERVER="localhost/apiv2"
CLIENT_ORIGIN='http://localhost:3000'
MAIL_USE_TLS=False
MAIL_SERVER=""
MAIL_PORT=""
MAIL_USERNAME=""
MAIL_PASSWORD=""
SECURITY_SEND_REGISTER_EMAIL=False
OAUTHLIB_INSECURE_TRANSPORT=False
SECURITY_EMAIL_SENDER=""
GOOGLE_OAUTH_CLIENT_ID=""
GOOGLE_OAUTH_CLIENT_SECRET=""
```

Las variables POSTGRES_HOST/POSTGRES_USER y CLIENT_ORIGIN son muy importantes para el funcionamiento mínimo.


- Postgresql

Usaremos docker para conseguir acceso al servicio de postgresql.

``` sh
sudo docker-compose up -d db
```

- Api

Poetry (asegurese de tener instalado Python):

``` sh
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - 
```

Ahora podemos iniciar el servicio local de Bioleft: 

``` sh
poetry shell
poetry install
alembic upgrade head
uvicorn backend.main:app --reload
```

Api docs: http://localhost:8000/docs

**Frontend:**

Primero, es necesario establecer la configuración de ambiente en ```frontend/.env ```:

```
VITE_API="http://localhost:8000"
VITE_STATIC_FOLDER="/static"
```

VITE_API: url de backend
VITE_STATIC_FOLDER: raíz de los archivos estáticos

- Nvm (opcional)

``` sh 
cd frontend && nvm install 16 && nvm use 16
``` 

- Nodejs y yarn

``` sh 
yarn
yarn run dev
``` 

*Ahora podés ingresar a http://localhost:3000*


## Cambiar instancia pública

**Imagen local**

Te permite crear una imagen de docker para poner en algun registro de docker para deployar en cualquier server.

Por ejemplo:
``` sh 
docker build -t bioleft/bioleft:3.0.0-pwa frontend
docker build -t bioleft/bioleft:3.0.0-api backend
ó
docker build -t registro.gitlab.com/bioleft/bioleft:3.0.0-pwa frontend
docker build -t registro.gitlab.com/bioleft/bioleft:3.0.0-api backend
``` 

**Publicación en servidor**

Con ansible se permite modificar la versión de la instancia para actualziar el sistema.


Por ejemplo:
``` sh 
git clone https://gitlab.com/bioleft/devs/bioleft-ansible
ansible-playbook deploy-bioleft.yaml
``` 
