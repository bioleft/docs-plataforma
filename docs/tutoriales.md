# Tutoriales

## Como introducirte en la Plataforma Bioleft

Bienvenida/o a la plataforma Bioleft.

- Para empezar, entrá al link: https://plataforma.bioleft.org.
- Al principio, hay 2 diferentes caminos: Iniciar sesión y ver el catálogo de semillas.
- Hacé clic en Iniciar sesión.
- Hacé clic en Registrarme, debajo del botón Continuar.
- Completá el formulario de registro principal, que consiste en elegir un nombre de usuario (que no contenga espacios ni caractéres especiales), un correo electrónico y una contraseña.
- Completá con tu nombre y ubicación principalmente.
- Hacé un recorrido por Semillas, Comunidad y Proyectos.
- Para usos de mejoramiento e intercambio de semillas, mirá los tutoriales para registrar una semilla o solicitar una y para registrar tu cultivo en un cuaderno de campo.

## Cómo registrar una semilla

Para la plataforma, las semillas permiten visibiliar información valiosa y a la vez permite tomar decisiónes como transferir a alguien y hacer seguimiento del mejoramiento.

Podés ver tus semillas en el menú Mis Semillas y ver las de otros usuarios en el catálogo, por la actividad de la comunidad y en los proyectos de mejoramiento.

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Hacé clic en el botón + en la barra superior.
- Ingresá los campos que se piden para registrar. Todas las semillas que se intercambian a través de Bioleft son abiertas, es decir sin restricciones para usos futuros de su progenie y semillas derivadas.
- Completar información de transferencia: términos del intercambio, cantidades y unidades ofrecidas. Esta información se puede editar con posterioridad. 
- Completar las condiciones bajo las cuales se transferirán las semillas: por ejemplo, retribución en semillas o  intercambio económico.

Es importante registrar la cantidad deseada a transferir. Puede ser como valor total o por cada una. No es estricto. Es necesario elegir las unidades de medida  también (kilos, paquetes, unidades, etc.).

La plataforma contribuye a la mejora constante de información sobre las especies de plantas y por ello presenta un espacio para la elaboración de esa inforamción, incluyendo formularios de  ficha técnica. La caracterización implementada, es mejorable y editable desde el corazón mismo de la plataforma.

## Cómo solicitar una semilla

Podés generar un proceso de interacambio o simplemente de solicitud de semilla que se encuentran en Mis transferencias.

Para los proyectos de mejoramiento participativo, para el registro de datos de campo, es necesario que la semilla esté en la plataforma y que esté en tu lista. Si estás en un proyecto, primero se tiene que hacer este proceso.

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Podes accedesr a ver los perfiles de las semillas desde distintos lugares pero lo simple es ir al catálogo de semillas en el menú superior. 
- Buscá la semilla y elegí Solicitar.
- Completá el formulario que se muestra.
- Hacé seguimiento desde Mis transferencias.

## Cómo usar las transferencias

En la sección Mis transferencias, podés tomar desiciónes y ver información generada por solicitudes que enviaste y recibiste.

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Hacé clic en el menú con tu avatar en la parte superior y luego clic en Mis transferencias.
- En el menú secundario podés elegir entre ver solicitudes enviadas y solicitudes recibidas.
- En solicitudes enviadas, también hay un filtro sobre los estados de las solicitudes.
- Podés seleccionar distintas solicitudes y marcarlas como completas o las podés completas una a una.

Las solicitudes completas permiten que los solicitantes puedan ver sus semillas en la plataforma y hacer seguimiento o realizar registros de cultivos en los cuadernos de campo de los proyectos de mejoramiento.

## Cómo hacer cuadernos de campo y proyectos de mejoramiento participativo

Una de las funcionalidades más importantes de la plataforma es la posibilidad de sistematizar el trabajo de campo de forma participativa, creando y registrando información de los cultivos de los ensayos. Esto significa que tenés la posibilidad de compartir tus observaciónes con otras personas o grupos.

Cada cuaderno inicia con una serie de datos a tomar (una planilla predeterminada) que están preparados según el cultivo que elijas. Esta planilla de datos es elaborada conjuntamente desde Bioleft. Vas a poder editar esta planilla siempre que quieras.

Para poder empezar:

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Hacé clic en Proyectos.
- Hacé clic en Comenzar nuevo proyecto.
- Completá el formulario.
- Elegí entre comenzar a registrar información de los ensayos ó ir a la sección principal del proyecto donde podés sumar personas y editar las observaciónes a registrar.

## Cómo registrar un ensayo en un proyecto de mejoramiento

Podés participar de proyectos de mejoramiento que consisten en tener un cuaderno de campo, con ensayos de cultivos sistematizados con información valiosa. Cada cultivo contiene una plantilla de datos de campo para ingresar. Y cada proyecto tiene la posibilidad de ingresar darle acceso a otras personas a compartir su información.

Para cargar la información del cultivo, es necesario que exista un cuaderno de campo en donde se guardarán de forma sistematizada. Si ya creaste o otros crearon el proyecto podés saltar esta parte.

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Hace clic en Proyectos en el menú superior.
- Elegir cuaderno donde ingresar el ensayo del cultivo. Si no hay cuaderno, entonces hacé clic en Comenzar nuevo proyecto.
- Buscá la semilla y elegí Solicitar.
- Completá el formulario que se muestra.
- Hacé seguimiento desde Mis transferencias.

## Cómo crear una organizacion y registrar una semilla

Podés crear una organización que pueda llevar a su nombre una semilla o un proyecto. Podés invitar a otros usuarios a la organizcion y darles acceso a los usos.

- Entrá a la plataforma: https://plataforma.bioleft.org.
- Hacé clic en el menú con tu avatar en la parte superior y luego clic en Mis organizaciones.
- Elegir cuaderno donde ingresar el ensayo del cultivo. Si no hay cuaderno, entonces hacé clic en Comenzar nuevo proyecto.
- Buscá la semilla y elegí Solicitar.
- Completá el formulario que se muestra.
- Hacé seguimiento desde Mis transferencias.
