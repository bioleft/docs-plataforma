# Manuales Bioleft

Bienvenido/a a la plataforma web Bioleft. Este es el sitio donde se encuentran las guías prácticas de usuario y documentación útil para entender como abordar cada aplicación.

La plataforma Bioleft es una herramienta para mapear semillas de código abierto y para hacer mejoramiento a travéz de la recolección de datos de campo en cuadernos de campo y en proyectos participativos.

- Podés ver [tutoriales](/tutoriales) prácticos para utilizar la plataforma.
- Podés acceder al [manual](/manual) compelto.
