# Manual

## Licencias

Bioleft propone un instrumento legal para la transferencia de material genético (semillas) que garantice que dicho material permanecerá disponible libremente para fines de investigación y desarrollo y para el registro de nuevas variedades de semillas. Este instrumento funciona paralelamente a la legislación existente en materia de propiedad intelectual, como una licencia o como una cláusula que podría añadirse a un contrato. Es flexible en términos de condiciones de uso, pero siempre incluirá una cláusula viral que asegura que las semillas mejoradas, obtenidas a partir de material Bioleft, también serán Bioleft, es decir, que también estarán disponibles para investigación y desarrollo y para el registro de nuevas variedades.

Las 3 licencias:

##### Multiplicacion abierta
  - Se permite el uso para investigación, desarrollo y registro de nuevas variedades
  - Se permite el guardado de la semilla para uso propio.
  - Se permite la multiplicación y venta, donación o intercambio posterior de la semilla tal como es, sin mejoras genéticas.

##### Multiplicacion exclusiva
  - Se permite el uso para investigación, desarrollo y registro de nuevas variedades.
  - Se permite el guardado de la semilla para uso propio.
  - La multiplicación para venta/ donación/ intercambio es posible sólo bajo autorización expresa del proveedor.

##### Sin multiplicacion
  - Se permite el uso para investigación, desarrollo y registro de nuevas variedades.
  - Se permite el guardado de la semilla para uso propio.
  - No se permite multiplicar la semilla.

Información: https://bioleft.org/licencias/

## Usos de la plataforma

### Catálogo de semillas de código abierto

La plataforma intenta resolver casos de uso donde usuarios y organizaciónes comparten semillas de código abierto, entonces la semilla es registrada, catálogada, con ficha técnica según el cultivo y la información básica como cultivar, detalle del material e imágenes representativas. Se establecen las cantidades disponible de compartir junto a los términos de transferencia y las solicitudes son generadas apartir de estos procesos.

### La actividad de la comunidad

Todos los usuarios y las organizaciónes aparecen en la sección Comunidad. Existe un canal de comunicación que permite categorizar y etiquetar mensajes.

### Información de campo

En lo que corresponde a la sistematización de la información de campo. Desde Proyectos podés sistematizar informacón sobre la actividad de los cultivos que realices con las semillas que dispongas, en forma grupal o individual.

Los cultivos tienen plantillas de datos que son elaboradas conjuntamente dentro del equipo de Bioleft y se pueden ver [acá](codigo). Estos formularios son importantes para los ensayos. 
Los ensayos se completan con datos de ambiente, con semillas y los resultados de la cada campo del formulario.

## Usuarios y comunidad

Podés acceder a la plataforma si realizaste el registro.

### Registrarse en la plataforma

El registro es un formulario que requiere:

- Nombre de usuario
- Correo electrónico
- Contraseña

### Configuración de perfil de usuario

Solicitamos completar tu perfil, ya que esta información es muy valiosa para intercambiar semillas y participar en proyectos de mejoramiento participativo.

Después de la creación de la cuenta, tenés que completar *nombre* y *ubicación* ya que son requeridos.

Desde el perfil de usuario se puede acceder al formulario de información.

## Semillas

La plataforma Bioleft permite que cualquier usuarix pueda registrar semillas de código abierto en el catálogo y además dispone de funcionalidades para que los usuarixs puedan compartirlas y mejorarlas. Todos los procesos los pueden realizar cualquier usuarix siempre que tenga los permisos.

### Registro de semilla

Las semillas que se registran tienen para definir distintos datos. Los datos que componen el registro principal son cultivar, registrante, obtentor, licencia y cantidades para compartir / intercambiar. Posteriormente podés registrar información técnica más detallada según el cultivo.

Los pasos para registrar una semilla son:
    
- Clic en el botón más (+) de el menú superior.
- Completar el formulario: cultivar, foto,  tipo de semilla y licencia. Todas las semillas que se intercambian a través de Bioleft son abiertas, es decir sin restricciones para usos futuros de su progenie y semillas derivadas.
- Completar información de transferencia: términos del intercambio, cantidades  y unidades ofrecidas. Esta información se puede editar con posterioridad. 
- Completar las condiciones bajo las cuales se transferirán las semillas: por ejemplo, retribución en semillas o  intercambio económico.

### Disponibilidad de semillas

Podés realizar tareas en la plataforma que tienen que ver con las transferencias e intercambios. Esto quiere decir que el usuarix u organización que registra la semilla puede establecer cantidades y términos para transferir siempre que quiera y otros usuarixs pueden solicitar semillas aceptando esas cantidades y esos términos. En Mis Transferencias, podés ver las solicitudes, aceptarlas o rechazarlas. Las transferencias que se aceptan, hacen que quien solicita pueda disponer de esa semilla en la plataforma para otras operaciones (transferir, ingresar información de campo, etc).

Para poner la disponibilidad tenés que hacer:

- Ingresá al perfil de la semilla
- Hacé clic en Intercambios
- Completá el formulario

Luego, cualquier usuarix que quiera solicitar la semilla, tiene que ingresar la cantidad y aceptar los términos de transferencia.

### Ficha técnica

La semilla cuenta con ficha técnica para registrar. Los atributos son organizados por especie. 

Para usar esta función:

- Entrá al perfil de la semilla
- Hacé clic en Editar Ficha Técnica
- Completá los campos que necesites

## Transferencias

### Funciones de transferencias

Todos los registros de transferencias están en Mis transferencias. 

**Solicitudes recibidas**

Se ordenan según el estado: pendiente, aceptada o rechazada.

Las solicitudes de estado pendiente pueden ser aceptadas o rechazadas.

**Solicitudes enviadas**

Listado completo de semillas solicitadas por el usuario.

### Solicitud de semilla

La solicitud de una semilla es el proceso en donde se hacen pedidos según cantidades y términos de transferencia.

Para solicitar:

- Ingresar al catálogo de semillas y elegir la semilla que se quiere solicitar.
- Una vez elegida la semilla, hacer clic en el botón Solicitar. También se puede entrar al perfil y hacer clic en el Solicitar. 
- Completar el formulario de transferencia. Aquí se encuentran las condiciones dispuestas por quien ofrece la semilla. 
- Hacé click en Completar. 
- Una vez finalizada la solicitud, tanto quien pide la semilla como quien la ofrece recibirán notificaciones por correo electrónico. 

## Cuadernos de campo y proyectos de mejoramiento

Compartir diferentes carácteristicas, métodos y objetivos de los cultivos es de los usos principales de la plataforma. Es posible realizar mejoramiento (plan breeding) sistematizado que permita participar a usuarios y organizaciones en un mismo espacio. 

Un cuaderno de campo es un conjunto de datos tomados durante todo el proceso de desarrollo del cultivo. Un proyecto de mejoramiento permite establecer objetivos entre diferentes usuarios y organizaciones.

Un ensayo es donde los usuarios registran información de los cultivos.

### Comenzar

Para eso:

- Hacé clic en el menú Proyectos en la barra superior
- Completá el formulario sobre el cuaderno de campo y proyecto que vas a crear
- Hacé clic en Comenzar ensayo
- Registrá información de campo

### Registro de ensayos

La información recolectada de los cultivos se puede registrar como ensayos en los proyectos de mejoramiento. 

- Ir al proyecto de mejoramiento
- Hacé clic en Comenzar nuevo ensayo

La primera información del ensayo es: Ambiente y Semillas. Luego, los campos son los establecidos en la planilla de variables.

### Variables de observación

La información del cultivo está compuesta por variables (fecha de plantación, cantidad de hojas, insectos, etc) que es editable. Cuando el proyecto es creado, hay variables que se establecen automaticamente según la especie. Estas variables pueden tambien editarse dentro del código fuente de la plataforma, si quiere participar de la construcción, escriba a info@bioleft.org.

Para editar las variables:

- Entrá al proyecto.
- Hacé clic en el botón Editar.
- En la parte de la planilla podés crear una nueva variable o editar alguna:
  - Nombre de la variable: este es el nombre que le daremos al dato por ejemplo: rendimiento, sabor, altura de la planta, enfermedades, etc.
  - Descripción de la variable.
  - Categoría: esto ayuda a entender que puede ser agrupado de cierta manera, por ejemplo: variable única, que se repite, que pertenece a una etapa del cultivo. Actualmente está sin uso por el momento.
  - Formato: de tipo texto, numérico, imagen, de selección única o múltiple, etc. Ayudará a que no cambie de formato, que se pueda analizar y que su carga visual sea más óptima.
  - Opciones de selección: en caso de que el formato sea de selección.

### Invitá personas

Unir usuarios con proyectos es una parte en la cual se permiten intercambiar informaciónes de cultivos. Para ello los usuarios que se unan deberán acceder a los materiales que se estén mejorando o ensayando. Las transferencias son generadas por el usuario que invita.

- Entrá al proyecto
- Hacé clic en Editar
- Hacé clic en Agregar
- Luego ingresar el correo electrónico
- Seleccionar el usuario
- Clic en Completar

## Comunidad

La sección Comunidad contiene dos partes de la actividad de usuarios y organizaciones: un mapeo y listado tipo contactos y un foro de discusión para la elaboración de temas.

### Uso de Comunidad

- Hacé clic en el menú Comunidad.
- Puede utilizar, el buscador y las etiquetas rapidas de filtro: Personas, Organizaciones, Con proyectos y lista de semillas.
- El mapa encuentra y marca los puntos por Localidad. Esos puntos pueden operar como filtros tambien si se le hace clic.
- La lista de usuarios y organizaciónes se puede desplegar y comprimir. Solo se puede acceder al perfil con los datos que se brindan.

### Uso del Foro

- Dentro de Comunidad, hay un botón en le barra superior que hace referencia a modificar la ventana entre Comunidad y Foro. Hacer clic en Foro.
- Aquí se encuentra:
	- Acceso a registrar nuevo tema
		- Para registrar un nuevo tema, deberá completar el formulario: seleccionar una categoría, poner título y contenido.
	- Acceso a los temas, agrupados en dos categoría.
- Los temas tienen una columna superior donde, a la izquierda está el titulo y a la derecha hay botónes de acción sobre ese tema.
	- Editar tema: solo el responsable del mensaje
	- Dejar mensaje: se accede a responder al tema.
	- Eliminar tema: solo el responsable del mensaje elimina el tema y las respuestas.
- Todas las respuestas tienen la acción de eliminar por el responsable.

## Organizaciones

La plataforma tiene la posibilidad de hacer seguimiento de la actividad de una organización permitiendole a los usuarios registrar semillas y proyetos. Cuenta también con diferentes campos sobre la información de la organización que permite visibilizarse.

Para crear una organización:

- Hacer clic en Registrar organización que es representado por un icono Más.
- Completar el formulario.
